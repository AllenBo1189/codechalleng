# prerequisites

- install [nodejs](https://nodejs.org/)
- install serverless framework globally (`npm i serverless -g`)
- AWS credential account and credential for deployment

# project structure

    ├── data/
    │   ├── dealers.json
    │   └── vehicles.json
    ├── handler.js
    ├── serverless.yml

- in data folder, dealers and vehicles data are saved in JSON format, later a persistent database can be used to conduct CRUD operations
- in handler.js, function code will be executed when certain events triggered, in this case handler http request
- serverless.yml, service (project info), provider (cloud provider info) and resources (AWS infrastructure resources)

once the project is fully deployed, the overall solution architecture diagram is like below
![SAD](./assets/SAD.png)

## evaluate and optimise package.json

run `npm install` and see vulnerabilities packages
![npm vulnerabilities](./assets/npm-vulnerabilities.png)

### upgrade dependencies versions

- remove `lodash` to reduce dependencies size, update `handler.js` with ES6 native array methods
- move `serverless-offline` and `serverless-dotenv-plugin` in dev-dependencies

```SHELL
`npm i -D serverless-dotenv-plugin serverless-offline`
`npm i serverless`
```

after optimisation, the dependencies is like below

![npm package update](./assets/npm-package-update.png)

## run locally offline

```SHELL
npm run offline
```

warnings and key info are printed out
![offline warning](./assets/offline-warning.png)
![offline info](./assets/offline-info.png)

# a few actions includes

- update the runtime from `nodejs8.10` (in serverless.yml) to `nodejs14.x`
- add .env file, and give STAGE value is development

# other things to consider

- for multiple stages/environments, consider providing different `.env` files, like `.env.development`, `.env.production`
- for latest version of serverless-offline, `port` is deprecated and `httpPort` is used. if not using default 3000 port, either update `serverless.yaml` or update `package.json`
- for serverless command, `-v` is deprecated, `--verbose` is used
- .env should be placed in .gitignore

## send requests

data are retrieved with 200 OK response

GET | http://localhost:4001/development/dealers
![GET-dealers](./assets/GET-dealers.png)
GET | http://localhost:4001/development/vehicles/122345
![GET-vehicles](./assets/GET-vehicles.png)

# serverless.yml

4 resources are explicitly created

- api gateway (ApiGatewayRestApi)
- IAM role (CNXCodeTestRole)
- lambda function (get-dealers)
- lambda function (get-vehicles-by-bac)

their usage and purpose:

- API gateway is used to receive http requests
- IAM role is assigned to lambda functions and with permission to create and write cloudwatch logs
- lambda functions are used to find data and return it back

# handler.js optimisation

- move require statement to the top
- update `requestHeaderGen` to handler 404

# deployment

```SHELL
export AWS_ACCESS_KEY_ID=<your-key-here>
export AWS_SECRET_ACCESS_KEY=<your-secret-key-here>
npm run deploy
```

![deploy1](./assets/deploy1.png)
![deploy2](./assets/deploy2.png)

# testing

- install dependency

  ```SHELL
  npm i -D jest
  ```

- run tests

  ```SHELL
  npm run test
  ```

test coverage as below
![test-coverage](./assets/test-coverage.png)
