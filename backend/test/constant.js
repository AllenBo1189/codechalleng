module.exports.getDealersPositive = {
  statusCode: 200,
  body: '[{"bac":"122345","name":"Cadillac Detriot","city":"Detriot","state":"WV","country":"US","brand":"Cadillac"},{"bac":"122346","name":"Buick Detriot","city":"Detriot","state":"WV","country":"US","brand":"Buick"},{"bac":"122347","name":"GMC Detriot","city":"Detriot","state":"WV","country":"US","brand":"GMC"},{"bac":"122348","name":"Buick New York","city":"New York","state":"WV","country":"US","brand":"Buick"}]',
  headers: {
    'Content-Type': 'application/json',
    'X-Requested-With': '*',
    'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,x-requested-with',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST,GET,OPTIONS,PUT',
  },
  isBase64Encoded: false,
};

module.exports.getVehiclesPositive = {
  statusCode: 200,
  body: '{"_id":"5ba47ea11e867b8c0ac40c9d","bac":"122345","vin":"VIN00000000000000","ctpStatus":"IN-SERVICE","onstarStatus":"ONS-116","events":[{"_id":"5ba47ea11e867b8c0ac40c9e","eventDate":"2018-09-19T14:00:00.000+0000","eventType":"created"}],"createdAt":"2018-09-21T05:16:17.927+0000","updatedAt":"2018-10-09T02:50:29.624+0000","make":"Cadillac","model":"T","telemetryPnid":"67890","color":"Black","stockNumber":"12345","year":2018}',
  headers: {
    'Content-Type': 'application/json',
    'X-Requested-With': '*',
    'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,x-requested-with',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST,GET,OPTIONS,PUT',
  },
  isBase64Encoded: false,
};

module.exports.internalErrorResponse = {
  statusCode: 500,
  body: '{"message":"Internal Server Error. Please try again"}',
  headers: {
    'Content-Type': 'application/json',
    'X-Requested-With': '*',
    'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,x-requested-with',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST,GET,OPTIONS,PUT',
  },
  isBase64Encoded: false,
};
module.exports.notFoundResponse = {
  statusCode: 404,
  body: '"Resource Not Found. Please try again"',
  headers: {
    'Content-Type': 'application/json',
    'X-Requested-With': '*',
    'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,x-requested-with',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST,GET,OPTIONS,PUT',
  },
  isBase64Encoded: false,
};
