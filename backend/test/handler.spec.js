const { getDealers, getVehiclesByBac } = require('../handler');
const { getDealersPositive, internalErrorResponse, getVehiclesPositive, notFoundResponse } = require('./constant');

describe('getDealers', () => {
  beforeEach(() => {
    jest.spyOn(global.Math, 'random').mockRestore();
  });
  it('200 response case', async () => {
    jest.spyOn(global.Math, 'random').mockReturnValue(0.5);
    const result = await getDealers();
    expect(result).toEqual(getDealersPositive);
  });
  it('500 response case', async () => {
    jest.spyOn(global.Math, 'random').mockReturnValue(0.05);
    const result = await getDealers();
    expect(result).toEqual(internalErrorResponse);
  });
});

describe('getVehiclesByBac', () => {
  beforeEach(() => {
    jest.spyOn(global.Math, 'random').mockRestore();
  });
  it('200 response case', async () => {
    jest.spyOn(global.Math, 'random').mockReturnValue(0.5);
    const result = await getVehiclesByBac({ pathParameters: { bac: '122345' } });
    expect(result).toEqual(getVehiclesPositive);
  });
  it('404 response case', async () => {
    const result = await getVehiclesByBac({ pathParameters: { bac: '123456' } });
    expect(result).toEqual(notFoundResponse);
  });
  it('500 response case', async () => {
    jest.spyOn(global.Math, 'random').mockReturnValue(0.05);
    const result = await getVehiclesByBac({ pathParameters: { bac: '122345' } });
    expect(result).toEqual(internalErrorResponse);
  });
});
