# vue3-car-dealership

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

- (optional) update favicon.ico
- upload content inside dist folder into s3
- (optional) invalid cloudfront

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
