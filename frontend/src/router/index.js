import { createRouter, createWebHistory } from 'vue-router';
import Dealer from '../views/Dealer';
import Vehicle from '../views/Vehicle';

const routes = [
  {
    path: '/',
    name: 'Dealer',
    component: Dealer,
  },
  {
    path: '/vehicles/:id',
    name: 'Vehicle',
    component: Vehicle,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
